/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.task.config
 * @filename: CustomizableThreadPoolConfig.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.task.config;

/**
 * @typename: CustomizableThreadPoolConfig
 * @brief: 可定制线程池配置
 * @author: KI ZCQ
 * @date: 2018年4月28日 下午2:54:44
 * @version: 1.0.0
 * @since
 * 
 */
public class CustomizableThreadPoolConfig implements ThreadPoolConfig {

	private int coreTaskNum = CORE_TASK_NUM;
	private int maxTaskNum = MAX_TASK_NUM;
	private int maxCacheTaskNum = MAX_CACHE_TASK_NUM;
	private int queueFullSleepTime = QUEUE_FULL_SLEEP_TIME;
	private long taskTimeoutMillSeconds = TASK_TIMEOUT_MILL_SECONDS;

	/**
	 * coreTaskNum
	 *
	 * @return the coreTaskNum
	 * @since 1.0.0
	 */
	public int getCoreTaskNum() {
		return coreTaskNum;
	}

	/**
	 * @param coreTaskNum
	 *            the coreTaskNum to set
	 */
	public void setCoreTaskNum(int coreTaskNum) {
		this.coreTaskNum = coreTaskNum;
	}

	/**
	 * maxTaskNum
	 *
	 * @return the maxTaskNum
	 * @since 1.0.0
	 */
	public int getMaxTaskNum() {
		return maxTaskNum;
	}

	/**
	 * @param maxTaskNum
	 *            the maxTaskNum to set
	 */
	public void setMaxTaskNum(int maxTaskNum) {
		this.maxTaskNum = maxTaskNum;
	}

	/**
	 * maxCacheTaskNum
	 *
	 * @return the maxCacheTaskNum
	 * @since 1.0.0
	 */
	public int getMaxCacheTaskNum() {
		return maxCacheTaskNum;
	}

	/**
	 * @param maxCacheTaskNum
	 *            the maxCacheTaskNum to set
	 */
	public void setMaxCacheTaskNum(int maxCacheTaskNum) {
		this.maxCacheTaskNum = maxCacheTaskNum;
	}

	/**
	 * queueFullSleepTime
	 *
	 * @return the queueFullSleepTime
	 * @since 1.0.0
	 */
	public int getQueueFullSleepTime() {
		return queueFullSleepTime;
	}

	/**
	 * @param queueFullSleepTime
	 *            the queueFullSleepTime to set
	 */
	public void setQueueFullSleepTime(int queueFullSleepTime) {
		this.queueFullSleepTime = queueFullSleepTime;
	}

	/**
	 * taskTimeoutMillSeconds
	 *
	 * @return the taskTimeoutMillSeconds
	 * @since 1.0.0
	 */
	public long getTaskTimeoutMillSeconds() {
		return taskTimeoutMillSeconds;
	}

	/**
	 * @param taskTimeoutMillSeconds
	 *            the taskTimeoutMillSeconds to set
	 */
	public void setTaskTimeoutMillSeconds(long taskTimeoutMillSeconds) {
		this.taskTimeoutMillSeconds = taskTimeoutMillSeconds;
	}

}
