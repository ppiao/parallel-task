/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.test.spring
 * @filename: SumBigDataTask.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.test.spring;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import com.ngplat.paralleltask.forkjoin.AbstractRecursiveWorker;
import com.ngplat.paralleltask.task.TaskContext;

/**
 * @typename: SumBigDataTask
 * @brief: 〈一句话功能简述〉
 * @author: KI ZCQ
 * @date: 2018年6月5日 下午12:07:47
 * @version: 1.0.0
 * @since
 * 
 */
public class SumBigDataTask extends AbstractRecursiveWorker<Integer> {
	
	/**
	 * 创建一个新的实例 SumBigDataTask.
	 * 
	 * @param context
	 * @param list
	 * @param countDownLatch
	 */
	public SumBigDataTask(TaskContext context, List<Integer> list, CountDownLatch countDownLatch) {
		super(context, list, countDownLatch);
	}

	/**
	 * @see com.ngplat.paralleltask.forkjoin.RecursiveWorker#work(java.lang.Object)
	 */
	@Override
	public void work(TaskContext context) {
		Integer sum = 0;
		// 遍历累加
		for (int i = 0; i < calcList.size(); i++) {
			sum += calcList.get(i);
		}
		
		try {
			// 测试队列满的情况
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("[SumBigDataTask:work] taskId: " + taskId + ", sum: " + sum);
		// 存结果
		context.putResult(taskId, sum);
	}

}
