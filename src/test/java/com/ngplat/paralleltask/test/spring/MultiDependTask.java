/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.test.spring
 * @filename: MultiDependTask.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.test.spring;

import com.ngplat.paralleltask.annotation.TaskBean;
import com.ngplat.paralleltask.exception.TaskExecutionException;
import com.ngplat.paralleltask.task.TaskContext;
import com.ngplat.paralleltask.task.TaskProcessor;

/** 
 * @typename: MultiDependTask
 * @brief: 〈一句话功能简述〉
 * @author: KI ZCQ
 * @date: 2018年5月29日 上午11:03:51
 * @version: 1.0.0
 * @since
 * 
 */
@TaskBean(taskId = "5", name = "MultiDependTask", parentIds = {"1", "2", "3", "4"})
public class MultiDependTask extends TaskProcessor {

	// serialVersionUID
	private static final long serialVersionUID = -2234846038936550246L;

	/**
	 * 创建一个新的实例 MultiDependTask.
	 */
	public MultiDependTask() {
		// TODO Auto-generated constructor stub
	}

	/** 
	 * @see com.ngplat.paralleltask.task.TaskProcessor#runWorker(com.ngplat.paralleltask.task.TaskContext)
	 */
	@Override
	public void runWorker(TaskContext context) throws TaskExecutionException {
		System.out.println("[MultiDependTask] Run MultiDependTask.");
	}

}
